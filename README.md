<p align="center">
  <a href="https://www.dareosmeyer.com">
    <img alt="dareosMyr" src="https://res.cloudinary.com/dupmgz46v/image/upload/v1642023820/personal_logo.png" width="100" />
  </a>
</p>
<h1 align="center">
  Dareos Meyer's Portfolio Site
</h1>

Needing a portfolio, I designed and built a striking but uncluttered front-end to make visitors look twice and connected it to a robust content management system which delivers copy and media worthy of that second look. Strapi CMS allows me to easily edit any page, add new projects or experiments and manage SEO all without needing to dig back into the code.

#### Technologies
- Gatsby
- Strapi
- Heroku
- Vercel

#### Design
- Used Adobe Illustrator and Photoshop to make the custom assets like the nav bar background.
- Designed the UI in Figma
- Programmed a rain system which React(s) to the CTA button, the themes and the offsetHeight of each page.

