require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    title: `Dareos Meyer's Portfolio`,
    description: `If you are seeing this then the web dev (me) has made a boo boo.`,
    author: `dareosMyr`,
    siteUrl: `https://www.dareosmeyer.com`,
    defaultImage: `https://res.cloudinary.com/dupmgz46v/image/upload/v1641496244/personal%20site/static%20assets/default_ogimage_k3frko.png`,
    defaultImageAlt: `Pixel art grey clouds above a dark blue background with the text Dareos Meyer, Programmer Designer Writer.`,
  },

  plugins: [
    {
      resolve: `gatsby-source-strapi`,
      options: {
        apiURL: process.env.CMS_URL,
        queryLimit: 1000,
        collectionTypes: ["experiment", "project"],
        singleTypes: [
          "homepage",
          "contact",
          "footer",
          "experiment-page",
          "project-page",
        ],
      },
    },

    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-layout`,
      options: {
        component: require.resolve(`./src/components/layout.js`),
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        defaults: {
          breakpoints: [450, 800, 1200],
          placeholder: `blurred`,
        },
      },
    },
    // {
    //   resolve: `gatsby-plugin-manifest`,
    //   options: {
    //     name: `gatsby-starter-default`,
    //     short_name: `starter`,
    //     start_url: `/`,
    //     background_color: `#663399`,
    //     // This will impact how browsers show your PWA/website
    //     // https://css-tricks.com/meta-theme-color-and-trickery/
    //     // theme_color: `#663399`,
    //     display: `minimal-ui`,
    //     icon: `src/assets/logo.png`, // This path is relative to the root of the site.
    //   },
    // },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
